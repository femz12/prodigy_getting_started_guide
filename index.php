<!DOCTYPE html>

<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <link rel="icon" href="assets/img/favicon.png">

    <title>Documentation | Prodigy Learning Management System</title>

<!--    <link href="assets/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="assets/css/semantic.min.css" rel="stylesheet">
    
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/components/icon.min.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <link href="assets/css/style.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  </head>
  
  <body>

    <div class="image-bg-pro">
        <div class="ui attached borderless menu">
            <div class="ui fluid container">
                <a class="item nav-item-hover-no-bg header" href="/">
                    <img src="assets/img/logo.png" class="ui centered image main-logo"><span id="doc-label" class="ui left pointing label primary-color-bg box-shadow-class nav-item-sign-in-hover quicksand-font font-weight-600">Documentation</span>
                </a>

                <div class="right item micro-right-menu">
                    <div class="ui top right pointing dropdown item white primary-color-bg button quicksand-font font-weight-500 nav-item-sign-in-hover">
                        <span class="text">Menu</span>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <a href="https://prodigylms.com/" class="item quicksand-font primary-color font-weight-500 nav-item-hover">Home</a>
                            <div class="divider"></div>

                            <a href="https://prodigylms.com/login" class="item quicksand-font primary-color font-weight-500 nav-item-hover">Sign In</a>
                        </div>
                    </div>
                </div>

                <div class="right item main-right-menu">
                    <a href="https://prodigylms.com/" class="item quicksand-font font-weight-500 primary-color nav-item-hover">Home</a>
                    <a href="https://prodigylms.com/login" class="ui blue button quicksand-font font-weight-500 primary-color-bg nav-item-sign-in-hover">Sign In</a>
                </div>
            </div>
        </div>

        <div class="margin-tb-60">
    <!--            <div class="margin-tb-30">-->
            <div class="Aligner">
                <div class="ui two column centered stackable container grid">
                    <div class="center aligned column">
                        <div class="ui segment transparent-segment">
                            <img src="assets/img/pro.png" class="ui centered image animated bounceInLeft">
                            <h1 class="ui header font-weight-600 white text-shadow quicksand-font animated bounceInLeft">
                                Professionals
                            </h1>
                            <div>
                                <a href="/professionals" class="ui green large button box-shadow-class quicksand-font font-weight-500 nav-item-sign-in-hover">Documentation
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="center aligned column">
                        <div class="ui segment transparent-segment">
                            <img src="assets/img/acad.png" class="ui centered image animated bounceInRight">
                            <h1 class="ui header font-weight-600 white text-shadow quicksand-font animated bounceInRight">
                                Academics
                            </h1>
                            <div>
                                <a href="/academics" class="ui blue large button box-shadow-class quicksand-font font-weight-500 primary-color-bg nav-item-sign-in-hover">Documentation
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <!--            </div>-->
        </div>

        <footer class="primary-color-bg">
            <div class="ui vertical footer primary-color-bg inverted segment">
                <div class="ui container relaxed grid">
                    <div class="ui sixteen wide column">
                        <div class="ui right floated horizontal list">
                            <div class="item" href="#">© ProdigyLMS, <?php echo date("Y") ?>. All Rights Reserved</div>
                        </div>
                        <div class="ui horizontal inverted link list">
                            <a class="item" href="https://prodigylms.com/">Home</a>
                            <a class="item" href="https://prodigylms.com/login">Sign In</a>
                            <a class="item label show_contact white">Contact >></a>
                            <a class="item hidden_contact hidden font-weight-500">+234 (0) 813 9061 823</a>
                            <a class="item hidden_contact hidden font-weight-500">+234 (0) 817 5436 659</a>
                            <a class="item hidden_contact hidden font-weight-500">support@prodigylms.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

  
    <!-- JavaScripts
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/semantic.min.js"></script>
    
    <script src="assets/js/custom.js"></script>
    
  </body>
</html>
