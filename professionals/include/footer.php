<footer class="site-footer">
    <div class="container-fluid">
        <a id="scroll-up" href="#"><i class="fa fa-angle-up"></i></a>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <p>&copy; ProdigyLMS,  <?php echo date("Y"); ?>. All right reserved</p>
            </div>
            <div class="col-md-6 col-sm-6">
                <ul class="footer-menu">
                    <li><a href="https://www.prodigylms.com" target="_blank">ProdigyLMS</a></li>
                    <li><a href="#">Home</a></li>
                    <li><a href="../admin">Admin</a></li>
                    <li><a href="../tutor">Tutor</a></li>
                    <li><a href="../learner">Learner</a></li>
                    <li><a href="mailto:info@prodigylms.com">Contact us</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>