<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>Prodigy LMS | Documentation</title>

    <!-- Styles -->
    <!--<link href="assets/styles/bootstrap/css/bootstrap.css" rel="stylesheet">-->
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500" rel="stylesheet">

    <!-- Favicons -->
    <link rel="icon" href="public/img/favicon.png">
</head>

<body>
    <nav class="navbar navbar-fixed-top nav-bg">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed blue-bg" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="glyphicon glyphicon-align-justify"></span>
                </button>
                <a class="navbar-brand" href="/" style="margin-top: auto !important; margin-bottom: 10px !important;">
                    <img src="../assets/img/logo.png" alt="ProdigyLMS">
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="https://prodigylms.com/" target="_blank"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog"></span> Doc type <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="admin">Admin</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="tutor">Tutor</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="learner">Learner</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>

    <!-- Main content -->
    <div class="jumbotron image-bg">
        <div class="image-bg-overlay">
            <div class="container">
                <h1 class="text-shadow alternative-color font-w-500">Documentation</h1>
                <h5 class="text-justify tb-20-margin letter-space-1 font-w-500 sub-header line-height">
                    Prodigy's Learning Management System (LMS) allows you to create a professional online training portal to deliver e-learning your way.
                    From setting up a virtual classroom, to delivering videos and assessing student pass rates, Prodigy's LMS does it all. Prodigy provides the LMS platform: where you’ll own your own eLearning content.
                </h5>

                <div class="btn-group">
                    <a type="button" href="admin" class="btn btn-primary btn-md box-shadow-class font-w-500 letter-space-1">Administrator</a>
                    <a type="button" href="tutor" class="btn btn-danger btn-md box-shadow-class font-w-500 letter-space-1">Tutor</a>
                    <a type="button" href="learner" class="btn btn-warning btn-md box-shadow-class font-w-500 letter-space-1">Learner</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h3 class="text-center font-w-500 blue letter-space-1 basic-text-shadow"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> Welcome !</h3>
                <h5 class="text-center font-w-400 letter-space-1 md-line-height line-height">Just follow the links in the buttons to navigate to the respective Prodigy<span class="blue">LMS</span> documentation pages. It's that simple!</h5><br/><br/>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-10 col-md-offset-1">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="">
                            <img class="img-responsive auto-margin" style="height: 80px" src="public/img/admin_cogs.png">
                            <h4 class="text-center blue font-w-500 letter-space-1 basic-text-shadow">Administrator</h4>
                            <p class="text-center"><a href="admin" class="btn btn-primary btn-lg box-shadow-class" role="button"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Admin</a></p><br/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="">
                            <img class="img-responsive auto-margin" style="height: 80px" src="public/img/tutor.png">
                            <h4 class="text-center blue font-w-500 letter-space-1 basic-text-shadow">Tutor</h4>
                            <p class="text-center"><a href="tutor" class="btn btn-danger btn-lg box-shadow-class" role="button"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Tutor</a></p><br/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="">
                            <img class="img-responsive auto-margin" style="height: 80px" src="public/img/learner.png">
                            <h4 class="text-center blue font-w-500 letter-space-1 basic-text-shadow">Learner</h4>
                            <p class="text-center"><a href="learner" class="btn btn-warning btn-lg box-shadow-class" role="button"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Learner</a></p><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br/><br/>
    <!-- END Main content -->
    <!-- Footer -->
    <footer class="bd-footer text-muted">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ul class="footer-page-links">
                    <li><a href="https://prodigylms.com/" target="_blank">Home</a></li>
                    <li><a href="admin">Admin</a></li>
                    <li><a href="tutor">Tutor</a></li>
                    <li><a href="learner">Learner</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12">
                <ul class="footer-menu bd-footer-links">
                    <li><a href="https://prodigylms.com/" target="_blank">ProdigyLMS</a></li>
                    <li><a href="mailto:info@prodigylms.com">Contact us</a></li>
                    <li>&copy;  <?php echo date("Y"); ?> ProdigyLMS.</li>
                </ul>
            </div>
        </div>
    </footer>
<?php //include ('include/footer.html')?>
<!-- END Footer -->

<!-- Scripts -->
<script src="public/js/theDocs.all.min.js"></script>
<script src="public/js/custom.js"></script>
<script>
    //    @if(isset($jump_to))
    //       $(document).ready(function () {
    //           var jump_to='#'+$('#jump-to').val();
    //            $('html, body').animate({
    //                scrollTop:($(jump_to).offset().top)
    //            },100);
    //    });
    //    @endif
</script>
</body>
</html>

