<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title><?php echo  $page_title; ?></title>

    <!-- Styles -->
    <!--<link href="assets/styles/bootstrap/css/bootstrap.css" rel="stylesheet">-->
    <link href="../public/css/theDocs.all.min.css" rel="stylesheet">
    <link href="../public/css/custom.css" rel="stylesheet">

<!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500%7CLato:300,400' rel='stylesheet' type='text/css'>
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="../apple-touch-icon.html">
    <link rel="icon" href="assets/img/favicon.png">
</head>

<body data-spy="scroll" data-target=".sidebar" data-offset="200">
<!--@if(isset($jump_to))-->
<!--    <input type="hidden" value="{{$jump_to}}" id="jump-to"/>-->
<!--@endif-->
<?php
    if($user=='user')
        include 'sidebar.html';
    elseif($user=='tutor')
        include 'tutor_sidebar.html';
    elseif ($user=='learner')
        include 'learner_sidebar.html';
?>
<header class="site-header navbar-transparent">
    <!-- Top navbar & branding -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">

            <!-- Toggle buttons and brand -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
                    <span class="glyphicon glyphicon-option-vertical"></span>
                </button>

                <button type="button" class="navbar-toggle for-sidebar" data-toggle="offcanvas">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
            <!-- END Toggle buttons and brand -->

            <!-- Top navbar -->
            <!--<div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="banner">-->
            <!--<ul class="nav navbar-nav navbar-right">-->
            <!--<li class="active"><a href="index.html">Documentation</a></li>-->
            <!--<li><a href="page_blog.html">Blog</a></li>-->
            <!--<li><a href="page_faq.html">FAQ</a></li>-->
            <!--<li><a href="http://thethemeio.helpato.com/thedocs" target="_blank">Support</a></li>-->
            <!--<li class="hero"><a href="http://themeforest.net/item/thedocs-online-documentation-template/13070884?license=regular&amp;open_purchase_for_item_id=13070884&amp;purchasable=source&amp;ref=thethemeio">Purchase</a></li>-->
            <!--</ul>-->
            <!--</div>-->
            <!-- END Top navbar -->

        </div>
    </nav>
    <!-- END Top navbar & branding -->

    <!-- Banner -->
    <div class="banner auto-size" style="background-color: #5cc7b2">
        <div class="container-fluid">
            <?php echo $page_header; ?>
            <div class="btn-group">
                <a type="button" href="../admin" class="btn btn-primary btn-md box-shadow-class">Administrator</a>
                <a type="button" href="../tutor" class="btn btn-danger btn-md box-shadow-class">Tutor</a>
                <a type="button" href="../learner" class="btn btn-warning btn-md box-shadow-class">Learner</a>
            </div>
            <!--<h5>You can see an example of a banner in top of this page. Following code is the code for this banner.</h5>-->
        </div>
    </div>
    <!-- END Banner -->

</header>

<main class="container-fluid">
    <!-- Main content -->
    <article class="main-content" role="main">
        <?php include '../content/'.$page_content.'.html'; ?>
    </article>
    <!-- END Main content -->
</main>
<!-- Footer -->
<?php include 'footer.php'; ?>
<!-- END Footer -->

<!-- Scripts -->
<script src="../public/js/theDocs.all.min.js"></script>
<script src="../public/js/custom.js"></script>
<script>
</script>
</body>
</html>

